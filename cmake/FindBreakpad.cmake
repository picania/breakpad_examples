find_path(BREAKPAD_INCLUDE_DIR
    NAMES client/windows/handler/exception_handler.h
    HINTS ${BREAKPAD_ROOT}/src
)

# common
find_library(BREAKPAD_COMMON_DEBUG_LIB
    NAMES common
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Debug/lib
          ${BREAKPAD_ROOT}/build/src/Debug
          ${BREAKPAD_ROOT}/build/src/common/Debug
)

find_library(BREAKPAD_COMMON_RELEASE_LIB
    NAMES common
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Release/lib
          ${BREAKPAD_ROOT}/build/src/Release
          ${BREAKPAD_ROOT}/build/src/common/Release
)

# exception_handler
find_library(BREAKPAD_EXCEPTION_HANDLER_DEBUG_LIB
    NAMES exception_handler
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Debug/lib
          ${BREAKPAD_ROOT}/build/src/Debug
          ${BREAKPAD_ROOT}/build/src/client/windows/handler/Debug
)

find_library(BREAKPAD_EXCEPTION_HANDLER_RELEASE_LIB
    NAMES exception_handler
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Release/lib
          ${BREAKPAD_ROOT}/build/src/Release
          ${BREAKPAD_ROOT}/build/src/client/windows/handler/Release
)

# crash_generation_client
find_library(BREAKPAD_CRASH_GENERATION_CLIENT_DEBUG_LIB
    NAMES crash_generation_client
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Debug/lib
          ${BREAKPAD_ROOT}/build/src/Debug
          ${BREAKPAD_ROOT}/build/src/client/windows/crash_generation/Debug
)

find_library(BREAKPAD_CRASH_GENERATION_CLIENT_RELEASE_LIB
    NAMES crash_generation_client
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Release/lib
          ${BREAKPAD_ROOT}/build/src/Release
          ${BREAKPAD_ROOT}/build/src/client/windows/crash_generation/Release
)

find_library(BREAKPAD_CRASH_REPORT_SENDER_DEBUG_LIB
    NAMES crash_report_sender
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Debug/lib
          ${BREAKPAD_ROOT}/build/src/Debug
          ${BREAKPAD_ROOT}/build/src/client/windows/sender/Debug
)

find_library(BREAKPAD_CRASH_REPORT_SENDER_RELEASE_LIB
    NAMES crash_report_sender
    HINTS ${BREAKPAD_ROOT}/src/client/windows/Release/lib
          ${BREAKPAD_ROOT}/build/src/Release
          ${BREAKPAD_ROOT}/build/src/client/windows/sender/Release
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Breakpad DEFAULT_MSG BREAKPAD_INCLUDE_DIR)

if(BREAKPAD_FOUND)
    add_library(Breakpad::Breakpad INTERFACE IMPORTED)

    set_target_properties(Breakpad::Breakpad PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${BREAKPAD_INCLUDE_DIR})
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Debug>:${BREAKPAD_COMMON_DEBUG_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Release>:${BREAKPAD_COMMON_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:RelWithDebInfo>:${BREAKPAD_COMMON_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Debug>:${BREAKPAD_EXCEPTION_HANDLER_DEBUG_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Release>:${BREAKPAD_EXCEPTION_HANDLER_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:RelWithDebInfo>:${BREAKPAD_EXCEPTION_HANDLER_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Debug>:${BREAKPAD_CRASH_GENERATION_CLIENT_DEBUG_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Release>:${BREAKPAD_CRASH_GENERATION_CLIENT_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:RelWithDebInfo>:${BREAKPAD_CRASH_GENERATION_CLIENT_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Debug>:${BREAKPAD_CRASH_REPORT_SENDER_DEBUG_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:Release>:${BREAKPAD_CRASH_REPORT_SENDER_RELEASE_LIB}>)
    set_property(TARGET Breakpad::Breakpad APPEND PROPERTY
        INTERFACE_LINK_LIBRARIES $<$<CONFIG:RelWithDebInfo>:${BREAKPAD_CRASH_REPORT_SENDER_RELEASE_LIB}>)
endif()

