#include "failure.h"

#include <cstdio>
#include <stdexcept>

#include "abstract_class.h"

using std::runtime_error;

namespace failure
{

void pure_virtual_call()
{
	google_breakpad::Derived derived;
}

void nullptr_dereference()
{
	int* p = nullptr;

	*p = 1;
}

void invalid_parameter()
{
	std::printf(nullptr);
}

void throw_std_exception()
{
	throw runtime_error{"Catch me."};
}

}	// failure
