find_package(Threads REQUIRED)

add_executable(breakclient
    linux/main.cpp
)

target_include_directories(breakclient PRIVATE
    ${CMAKE_SOURCE_DIR}/3rd_party/breakpad
)

target_compile_options(breakclient PRIVATE
    $<$<COMPILE_LANGUAGE:CXX>:-std=c++14>
)

target_link_libraries(breakclient
    crash
    Threads::Threads
)
