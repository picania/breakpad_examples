#include <iostream>
#include <string>

#include <client/linux/handler/exception_handler.h>
#include "failure.h"

using std::cerr;
using std::wcerr;

static bool filterCallback(void* context)
{
    // тут можно решить, обрабатывать исключение, 
    // или пустить его дальше.
    return true;    // continue processing
}

static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor,
void* context, bool succeeded)
{
    // здесь записанный дамп можно отправить на сервер.
    // готового класса для отправки нет.
    cerr << "Dump path: " << descriptor.path() << '\n';
    return true;
}

using std::cout;
using std::string;

void usage(const std::string& program)
{
	cout << "Usage: " << program << " <digit>\n";
	cout << "where <digit>:\n";
	cout << "  1 - call 'invalid parameter' failure;\n";
	cout << "  2 - call 'null pointer dereference' failure;\n";
	cout << "  3 - call 'pure virtual call' failure;\n";
}

int main(int argc, char** argv)
{
    google_breakpad::MinidumpDescriptor descriptor{"dumps"};
    google_breakpad::ExceptionHandler eh{
        descriptor, filterCallback, dumpCallback, nullptr, true, -1};
    
	if (argc == 1)
	{
		usage(argv[0]);
		return 0;
	}

	const std::string raw{argv[1]};
	const int digit = std::atoi(raw.c_str());

	switch(digit)
	{
	case 1:
		failure::invalid_parameter();
		break;
	case 2:
		failure::nullptr_dereference();
		break;
	case 3:
		failure::pure_virtual_call();
		break;
	default:
		cout << "Wrong digit.\n";
		usage(argv[0]);
	}

	return 0;
}
