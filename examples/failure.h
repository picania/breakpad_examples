#pragma once

namespace failure
{

void pure_virtual_call();
void nullptr_dereference();
void invalid_parameter();
void throw_std_exception();

}	// failure
