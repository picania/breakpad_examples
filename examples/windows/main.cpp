#include <iostream>
#include <string>

#include <client/windows/handler/exception_handler.h>
#include <client/windows/sender/crash_report_sender.h>
#include "failure.h"

using std::cerr;
using std::wcerr;

bool Filter(void* context, EXCEPTION_POINTERS* exinfo,
	MDRawAssertionInfo* assertion)
{
	// Тут можно фильтровать пойманные исключения
	// true - исключение передается дальше для обработки классом ExceptionHandler
	// false - исключение сразу же выбрасывается как не обработанное и пусть
	// с ним разбирается кто-то другой.
	cerr << "Exception filtered...\n";
	return true;
}

bool Finish(const wchar_t* dump_path, const wchar_t* minidump_id,
	void* context, EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion,
	bool succeeded)
{
	cerr << "Exception handled...\n";

	// Отсылаем отчет о падении на сервер
	// Создаём CrashReportSender, указав checkpoint file, где будет храниться
	// количество уже отправленных за этот день отчетов,
	// чтобы соблюдать ограничения
	google_breakpad::CrashReportSender sender(L"crash.checkpoint");
	
	// Устанавливаем максимальное количество отосланных одним пользователем
	// отчетов в день.
	// По умолчанию (-1) - неограниченное число отчетов.
	sender.set_max_reports_per_day(-1);
	
	// В этой мапе можно передать дополнительные параметры
	std::map<std::wstring, std::wstring> params{
		{L"prod", L"breakclient"},
		{L"ver", L"1.0"},
		{L"minidump_id", minidump_id}
	};
	
	// В коллбэк имя сгенерированого файла дампа не приходит, но мы можем
	// его сконструировать
	std::wstring dos_filename = dump_path;
	dos_filename += L"\\";
	dos_filename += minidump_id;
	dos_filename += L".dmp";

	std::wstring unix_filename = dump_path;
	unix_filename.erase(unix_filename.size() - 1);
	unix_filename += L"/";
	unix_filename += minidump_id;
	unix_filename += L".dmp";

	wcerr << unix_filename << '\n';

	std::map<std::wstring, std::wstring> files{{{L"TEST"}, dos_filename}};
	
	// Наконец, отсылаем репорт на сервер
	google_breakpad::ReportResult r = sender.SendCrashReport(
		L"http://192.168.1.48/dumps/", params, files, 0
	);

	// Говорим юзеру, получилось, или нет
	if (r == google_breakpad::RESULT_SUCCEEDED)
	{
		cerr << "Crash report was sent. Thank you!\n";
	}
	else
	{
		cerr << "Could not send crash report. Thank you for trying, though!\n";
	}

	return true;
}

using std::cout;
using std::string;

void usage(const std::string& program)
{
	cout << "Usage: " << program << " <digit>\n";
	cout << "where <digit>:\n";
	cout << "  1 - call 'invalid parameter' failure;\n";
	cout << "  2 - call 'null pointer dereference' failure;\n";
	cout << "  3 - call 'pure virtual call' failure;\n";
	cout << "  4 - call 'c++ std exception' failure;\n";
}

int main(int argc, char** argv)
{
    auto* pHandler = new google_breakpad::ExceptionHandler( 
        L"dumps\\", 
        Filter, 
        Finish, 
        0, 
        google_breakpad::ExceptionHandler::HANDLER_ALL, 
        MiniDumpNormal, 
        L"", 
        0 );

	if (argc == 1)
	{
		usage(argv[0]);
		return 0;
	}

	const std::string raw{argv[1]};
	const int digit = std::atoi(raw.c_str());

	switch(digit)
	{
	case 1:
		failure::invalid_parameter();
		break;
	case 2:
		failure::nullptr_dereference();
		break;
	case 3:
		failure::pure_virtual_call();
		break;
	case 4:
		failure::throw_std_exception();
		break;
	default:
		cout << "Wrong digit.\n";
		usage(argv[0]);
	}

	return 0;
}
