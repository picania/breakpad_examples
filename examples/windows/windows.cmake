source_group(src FILES windows/main.cpp)
add_executable(breakclient
    windows/main.cpp
)

find_package(Breakpad REQUIRED)

target_link_libraries(breakclient
    crash
    Breakpad::Breakpad
    wininet.lib
)

set_target_properties(breakclient PROPERTIES
    LINK_FLAGS /LTCG
)
