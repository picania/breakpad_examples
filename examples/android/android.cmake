if (NOT ANDROID_HOME)
    set(ANDROID_HOME $ENV{ANDROID_HOME})

    if (NOT ANDROID_HOME)
        message(STATUS "Android SDK not found.")
        message(FATAL  "  Please define a cmake or environment variable: ANDROID_HOME")
    endif()
endif()

if (NOT JAVA_HOME)
    set(JAVA_HOME $ENV{JAVA_HOME})
    
    if (NOT JAVA_HOME)
        message(STATUS "Java SDK not found.")
        message(FATAL  "  Please define a cmake or environment variable: JAVA_HOME")
    endif()
endif()

find_package(Threads REQUIRED)

add_library(breakclient SHARED
    android/main.cpp
    ${ANDROID_NDK}/sources/android/native_app_glue/android_native_app_glue.c
)

set_target_properties(breakclient PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/${ANDROID_ABI}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib/${ANDROID_ABI}"
)

target_include_directories(breakclient PRIVATE
    ${ANDROID_NDK}/sources/android/native_app_glue
    "D:/develop/breakpad-cmake/src"
)

find_library(BREAKPAD 
    NAMES breakpad_client libbreakpad_client
    HINTS D:/develop/breakpad-cmake/build/src
    NO_DEFAULT_PATH
)

message(STATUS "Breakpad lib: ${BREAKPAD}")

target_link_libraries(breakclient
    crash
    D:/develop/breakpad-cmake/build/src/libbreakpad_client.a
    android
    log
    Threads::Threads
)

find_host_program(AAPT_EXEC
    NAMES aapt aapt.exe
    HINTS ${ANDROID_HOME}/build-tools/25.0.1)

message(STATUS "Android aapt tool: ${AAPT_EXEC}")

find_host_program(ZIPALIGN_EXEC
    NAMES zipalign zipalign.exe
    HINTS ${ANDROID_HOME}/build-tools/25.0.1)

message(STATUS "Android zipalign tool: ${ZIPALIGN_EXEC}")

find_host_program(KEYTOOL_EXEC
    NAMES keytool keytool.exe
    HINTS ${JAVA_HOME}/bin)

message(STATUS "Java keytool: ${KEYTOOL_EXEC}")

find_host_program(JARSIGNER_EXEC
    NAMES jarsigner jarsigner.exe
    HINTS ${JAVA_HOME}/bin)

message(STATUS "Java jarsigner tool: ${JARSIGNER_EXEC}")

add_custom_target(keystore_create
    COMMAND ${KEYTOOL_EXEC}
        -genkey
        -v
        -keystore ${CMAKE_BINARY_DIR}/debug.keystore
        -storepass android
        -alias androiddebugkey
        -dname "CN=company name, OU=organisational unit, O=organisation, L=location, S=state, C=country code"
        -keyalg RSA
        -keysize 2048
        -validity 20000
)

add_custom_target(apk_create
    COMMAND ${AAPT_EXEC} package -v -f
        -M ${CMAKE_CURRENT_SOURCE_DIR}/android/AndroidManifest.xml
        -S ${CMAKE_CURRENT_SOURCE_DIR}/android/res
        -I ${ANDROID_HOME}/platforms/android-${ANDROID_NATIVE_API_LEVEL}/android.jar
        -F ${CMAKE_BINARY_DIR}/breakclient.unsigned.apk
    
    COMMAND ${AAPT_EXEC} add
        ${CMAKE_BINARY_DIR}/breakclient.unsigned.apk lib/${ANDROID_ABI}/$<TARGET_FILE_NAME:breakclient>
    
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_dependencies(apk_create breakclient)

add_custom_target(apk_sign
    COMMAND ${JARSIGNER_EXEC}
        -sigalg SHA1withRSA
        -digestalg SHA1
        -storepass android
        -keypass android
        -keystore ${CMAKE_BINARY_DIR}/debug.keystore
        -signedjar ${CMAKE_BINARY_DIR}/breakclient.signed.apk
        ${CMAKE_BINARY_DIR}/breakclient.unsigned.apk
        androiddebugkey
)

add_dependencies(apk_sign apk_create)

add_custom_target(apk_align
    COMMAND ${ZIPALIGN_EXEC}
        -f
        -v 4
        ${CMAKE_BINARY_DIR}/breakclient.signed.apk
        ${CMAKE_BINARY_DIR}/breakclient.apk
)

add_dependencies(apk_align apk_sign)
